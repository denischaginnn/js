// Создать div
const div = document.createElement('div')

// Добавить к нему класс wrapper

div.classList.add('wrapper')

// Поместить его внутрь тэга body

const body = document.body
body.appendChild(div)


// Создать заголовок H1 "DOM (Document Object Model)"

const header = document.createElement('h1')
header.textContent = 'DOM (Document Object Model)'
// Добавить H1 перед DIV с классом wrapper

div.insertAdjacentElement('beforebegin', header)
// Создать список <ul></ul>



// Добавить в него 3 элемента с текстом "один, два, три"
const ul = `
<ul>
  <li>Один</li>
  <li>Два</li>
  <li>Три</li>
</ul>
`

// Поместить список внутрь элемента с классом wrapper
div.innerHTML = ul;
// =================================================
// Создать изображение
const img = document.createElement('img')


// Добавить следующие свойства к изображению
// 1. Добавить атрибут source

img.src = 'https://mimigram.ru/wp-content/uploads/2020/07/%D0%A7%D1%82%D0%BE-%D1%82%D0%B0%D0%BA%D0%BE%D0%B5-%D1%84%D0%BE%D1%82%D0%BE.jpeg'

// 2. Добавить атрибут width со значением 240

img.width = '240'

// 3. Добавить класс super

img.classList.add('super')

// 4. Добавить свойство alt со значением "Photo"

img.alt = 'Photo'

// Поместить изображение внутрь элемента с классом wrapper

div.appendChild(img)


// Используя HTML строку, создать DIV с классом 'pDiv' + c 2-мя параграфами
const elemHTML = `<div class='pDiv'>
    <p>Параграф 1</p>
    <p>Параграф 2</p>
</div>` 
// Поместить этот DIV до элемента <ul></ul>

const ulList = div.querySelector('ul')
ulList.insertAdjacentHTML('beforebegin', elemHTML)

// Добавить для 2-го параграфа класс text

const pDiv = div.querySelector('.pDiv')

const p2 = pDiv.children['1']
p2.classList.add('text')

// Удалить 1-й параграф

const p1 = pDiv.children['0'] // pDiv.firstElementChild.remove()
p1.remove()

// Создать функцию generateAutoCard, 


const generateAutoCard = (brand, color, year) => {
    const date = new Date();
    return `
        <div class="autoCard">
            <h2>${brand.toUpperCase()} ${year}</h2>
            <p>Автомобиль ${brand.toUpperCase()} - ${year} года. Возраст авто - ${date.getFullYear() - year} лет.</p>
            <p>Цвет: ${color}</p>
            <button type='button' class='btn' onClick={handleClick}>Удалить</button>
        </div>
    `;
}
// которая принимает 3 аргумента: brand, color, year

// Функция должна возвращать разметку HTML:
// <div class="autoCard">
//   <h2>BRAND YEAR</h2>
//   <p>Автомобиль BRAND - YEAR года. Возраст авто - YEARS лет.</p>
// </div>

// Создать новый DIV с классом autos

const autos = document.createElement('div')
autos.classList.add('autos')

// Создать 3 карточки авто, используя функцию generateAutoCard
const carsList = [
    {brand: 'Tesla', year: 2015, color: 'Красный'},
    {brand: 'Lexus', year: 2016, color: 'Серебристый'},
    {brand: 'Nissan', year: 2012, color: 'Черный'},
]
const cars = carsList.map(car => generateAutoCard(car.brand, car.color, car.year)).join('')
autos.innerHTML = cars

// Поместить эти 3 карточки внутрь DIV с классом autos
// Поместить DIV c классом autos на страницу DOM - до DIV с классом wrapper

div.insertAdjacentElement('beforebegin', autos)
// Добавить кнопку Удалить на каждую карточку авто

// При клике на кнопку - удалять карточку из структуры DOM

// 1. Выбрать все кнопки
const buttons = document.querySelectorAll('.btn');
console.log(buttons)
// 2. Создать функцию удаления
// 3. Использовать цикл - чтобы повесить обработчик события на каждую кнопку
function handleClick(e) {
    const currentButton = e.currentTarget;
    currentButton.closest('.autoCard').remove();
}
buttons.forEach(button => {
    button.addEventListener('click', handleClick)
})